//
//  CameraViewController.swift
//  Sempre
//
//  Created by Yash Rajana on 2/22/19.
//  Copyright © 2019 Yash Rajana. All rights reserved.
//

import UIKit
import AVFoundation

protocol NewPageDelegate {
    func updateWithNewPage(page: (lastMeasureNumber: Int, image: UIImage))
}
class CameraViewController: UIViewController, UINavigationControllerDelegate,  UIImagePickerControllerDelegate {
    
    
    var delegate: NewPageDelegate?
    
    var pageData: (lastMeasureNumber: Int, image: UIImage) = (lastMeasureNumber: 0, image: UIImage())
    
    @IBOutlet weak var measureTextField: UITextField!
    @IBOutlet weak var pageImage: UIImageView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let cameraAuthorizationStatus = AVCaptureDevice.authorizationStatus(for: .video)
        
        switch cameraAuthorizationStatus {
            case .notDetermined: requestCameraPermission()
            case .authorized: presentCamera()
            case .restricted, .denied: alertCameraAccessNeeded()
        }
        
    }
    
    func requestCameraPermission() {
        AVCaptureDevice.requestAccess(for: .video, completionHandler: {accessGranted in
            guard accessGranted == true else { return }
            self.presentCamera()
        })
    }
    
    @IBAction func saveTapped(_ sender: Any) {
        
        pageData.lastMeasureNumber = Int(measureTextField.text ?? "0") ?? 0
        
        self.delegate?.updateWithNewPage(page: (lastMeasureNumber: self.pageData.lastMeasureNumber , image: self.pageData.image))
        self.navigationController?.popViewController(animated: true)
        self.dismiss(animated: true, completion: nil)

    
        
    }
    func presentCamera() {
        let photoPicker = UIImagePickerController()
        photoPicker.sourceType = .camera
        photoPicker.delegate = self
        
        self.present(photoPicker, animated: true, completion: nil)
    }
    func alertCameraAccessNeeded() {
        let settingsAppURL = URL(string: UIApplication.openSettingsURLString)!
        
        let alert = UIAlertController(
            title: "Need Camera Access",
            message: "Camera access is required to make full use of this app.",
            preferredStyle: UIAlertController.Style.alert
        )
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
        alert.addAction(UIAlertAction(title: "Allow Camera", style: .cancel, handler: { (alert) -> Void in
            UIApplication.shared.open(settingsAppURL, options: [:], completionHandler: nil)
        }))
        
        present(alert, animated: true, completion: nil)
        
        
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
    
    
        picker.dismiss(animated: true)


        guard let image = info[.originalImage] as? UIImage else {
            print("No image found")
            return
        }

        self.pageImage.image = image
        self.pageData.image = image
    
    
    }
    
    @IBOutlet weak var imageView: UIImageView!
    let imagePicker = UIImagePickerController()
    
    @IBAction func loadImageButtonTapped(_ sender: UIButton) {
        imagePicker.allowsEditing = false
        imagePicker.sourceType = .photoLibrary
        
        present(imagePicker, animated: true, completion: nil)

    }
    
    @objc func imagePickerController2(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : AnyObject]) {
        if let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            imageView.contentMode = .scaleAspectFit
            imageView.image = pickedImage
        }
        
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    
    func imagePickerController2(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        var newImage: UIImage
        
        picker.dismiss(animated: true)
        
        guard let image = info[.originalImage] as? UIImage else {
            print("No image found")
            return
        }
        
        self.pageData.image = image
        
    }
        
     
    
    
    }
    


