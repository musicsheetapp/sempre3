//
//  HomeViewController.swift
//  Sempre
//
//  Created by Yash Rajana on 2/17/19.
//  Copyright © 2019 Yash Rajana. All rights reserved.
//

import UIKit
import CoreData


class HomeViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext

    var songs : [String] = []
    var songName: String!


    override func viewDidLoad() {
        super.viewDidLoad()
        

        tableView.delegate = self
        tableView.dataSource = self
        
        navigationController?.navigationBar.isHidden = true
       
    }
    
    @IBAction func plusButtonTapped(_ sender: Any) {
        self.performSegue(withIdentifier: "toNewSong", sender: nil)
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        
        self.loadSongs()
    }
    
    
    
    
    func loadSongs() {
        
        
        self.songs = []
        
        
        do {
            
            
            let savedSongs = try context.fetch(Song.fetchRequest())
            
            
            for savedSong in savedSongs {
                
                
                let song = savedSong as! Song
                self.songs.append(song.title ?? "")
                
                
            }
            
            
            self.tableView.reloadData()
            
            
        } catch {
            print(error as NSError)
        }
        
        
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Pass along song name to playviewcontroller
        if segue.identifier == "toPlayView" {
            let destination = segue.destination as! PlayViewController
            destination.songName = self.songName
        }
    }
    
    
    
}

    
        


extension HomeViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return songs.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.textLabel?.text = songs[indexPath.row]
        return cell
        
    
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: "toPlayView", sender: nil)
    }
}
