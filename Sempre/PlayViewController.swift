//
//  PlayViewController.swift
//  Sempre
//
//  Created by Yash Rajana on 2/22/19.
//  Copyright © 2019 Yash Rajana. All rights reserved.
//

import UIKit
import AVFoundation
import CoreData

class PlayViewController: UIViewController {
    
    @IBOutlet weak var musicImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var playButton: UIButton!
    
    var audioPlayer = AVAudioPlayer()
    
    var timer = Timer()
    var timerIsRunning = false
    
    var songName: String?
    
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    var pageMeasureNumbers: [Int] = []
    var beatsPerMeasure: Int16 = 0
    var pageImages: [UIImage] = []
    var beatsPerMinute: Int16 = 0
    
    var currentPage = 0
    var counter = 0
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        let sound = Bundle.main.path(forResource: "Click", ofType: "mp3")
        do {
            audioPlayer = try AVAudioPlayer(contentsOf: URL(fileURLWithPath: sound!))
        } catch {
            print(error)
        }
        
        self.loadSongData()
        
        // musicImageView.image = newImage
        
    }
    
    @IBAction func playButtonTapped(_ sender: Any) {
        
        if !timerIsRunning {
            
            // Start playing
            timer = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(PlayViewController.playMetSound), userInfo: nil, repeats: true)
            playButton.setTitle("Stop", for: .normal)
            
        } else {
            
            // Stop playing
            timer.invalidate()
            playButton.setTitle("Start", for: .normal)
            
        }
        
        timerIsRunning = !timerIsRunning
        
    }
    
    func loadSongData() {
        
        // Retrieve data about song using song name received from homeviewcontroller as predicate
        do {
            
            let fetchRequest: NSFetchRequest<Song> = Song.fetchRequest()
            fetchRequest.predicate = NSPredicate(format: "title == %@", songName ?? "")
            let songs = try context.fetch(fetchRequest)
            let song = songs.first!
            
            self.beatsPerMeasure = song.beatsPerMeasure
            self.beatsPerMinute = song.beatsPerMinute
            self.pageMeasureNumbers = song.pageMeasureNumbers
            self.pageImages = song.pageImages
            
        } catch {
            print(error as NSError)
        }
        
    }
    
    @objc func playMetSound() {
        
        audioPlayer.play()
        
        self.counter += 1
        if counter > pageMeasureNumbers[currentPage] * Int(beatsPerMeasure) {
            
            currentPage += 1
            counter = 0
            musicImageView.image = pageImages[currentPage]
            
            
        }
        
    }
    
}
