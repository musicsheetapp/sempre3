//
//  newSongViewController.swift
//  Sempre
//
//  Created by Yash Rajana on 2/17/19.
//  Copyright © 2019 Yash Rajana. All rights reserved.
//

import UIKit



class NewSongViewController: UIViewController, NewPageDelegate {
    
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext

    @IBOutlet weak var titleTextField: UITextField!
    @IBOutlet weak var composerTextField: UITextField!
    @IBOutlet weak var beatsPerMeasureTextField: UITextField!
  
    @IBOutlet weak var beatsPerMinuteTextField: UITextField!
    
    @IBOutlet weak var tableView: UITableView!
    
    var pages: [(lastMeasureNumber: Int, image: UIImage)] = []
        
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        // Do any additional setup after loading the view.
    }
    
    @IBAction func saveSong(_ sender: Any) {
        let song = Song(context: context)
        song.beatsPerMinute = Int16(beatsPerMinuteTextField.text ?? "0") ?? 0
        song.beatsPerMeasure = Int16(beatsPerMeasureTextField.text ?? "0") ?? 0
        song.composer = composerTextField.text
        song.title = titleTextField.text
        //not needed:
        //self.performSegue(withIdentifier: "SAVEGoToWelcome" , sender: nil)
        //:not needed
        for page in self.pages {
            
            var measureNumbers: [Int] = []
            measureNumbers.append(page.lastMeasureNumber)
            
            var images: [UIImage] = []
            images.append(page.image)
            
            song.pageMeasureNumbers = measureNumbers
            song.pageImages = images
            
        }
    }
    

    @IBAction func newPageTapped(_ sender: Any) {
        self.performSegue(withIdentifier: "toCameraView" , sender: nil)
    }
    
    func updateWithNewPage(page: (lastMeasureNumber: Int, image: UIImage)) {
        
        
        self.pages.append(page)
        self.tableView.reloadData()
        
        
        print(self.pages[0].lastMeasureNumber)
        
        
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    
    
        if segue.identifier == "toCameraView" {
    
    
            let destinationVC = segue.destination as! CameraViewController
            destinationVC.delegate = self
    
    
        }
        
    
    
    }
    

    
}

extension NewSongViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return pages.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        
        cell.textLabel?.text = String(indexPath.row)
        
        return cell
    }
    
    
}
